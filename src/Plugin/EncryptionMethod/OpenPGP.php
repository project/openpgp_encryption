<?php

namespace Drupal\openpgp_encryption\Plugin\EncryptionMethod;

use Drupal\encrypt\EncryptionMethodInterface;
use Drupal\encrypt\Plugin\EncryptionMethod\EncryptionMethodBase;

/**
 * OpenPGP Encryption class.
 *
 * @EncryptionMethod(
 *   id = "openpgp_encryption",
 *   title = @Translation("OpenPGP"),
 *   description = "OpenPGP encryption.",
 *   key_type_group = {"encryption"},
 *   can_decrypt = TRUE
 * )
 */
class OpenPGP extends EncryptionMethodBase implements EncryptionMethodInterface {

  /**
   * {@inheritdoc}
   */
  public function checkDependencies($text = NULL, $key = NULL) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function encrypt($text, $key, $options = []) {
    try {
      $public_key = \OpenPGP_Message::parse(\OpenPGP::unarmor($key, 'PGP PUBLIC KEY BLOCK'));
      $data = new \OpenPGP_LiteralDataPacket($text, ['format' => 'u']);
      $encrypted = \OpenPGP_Crypt_Symmetric::encrypt($public_key, new \OpenPGP_Message([$data]));
      return \OpenPGP::enarmor($encrypted->to_bytes(), 'PGP MESSAGE');
    }
    catch (\Throwable $ex) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($text, $key, $options = []) {
    try {
      $data = \OpenPGP_Message::parse(\OpenPGP::unarmor($text, 'PGP MESSAGE'));
      $private_key = \OpenPGP_Message::parse(\OpenPGP::unarmor($key, 'PGP PRIVATE KEY BLOCK'));
      $decryptor = new \OpenPGP_Crypt_RSA($private_key);
      $decrypted = $decryptor->decrypt($data);
      return $decrypted->packets[0]->data;
    }
    catch (\Throwable $ex) {
      return FALSE;
    }
  }

}
